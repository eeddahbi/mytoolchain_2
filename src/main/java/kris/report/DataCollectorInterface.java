package kris.report;

import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author Christian
 */
public interface DataCollectorInterface {
    public ArrayList<Map<String, Object>> collectData();
}
