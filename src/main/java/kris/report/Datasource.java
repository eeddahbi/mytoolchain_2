package kris.report;

import java.util.*;

/**
 *
 * @author Christian
 */
public class Datasource implements Iterable<Map<String,Object>>
{
    
    /**
     *Set of DatatCollectors
     */
    private ArrayList<DataCollectorInterface> dataCollectors = new ArrayList<>();
    /**
     * Internal storage
     */
    private ArrayList<Map<String,Object>> data = new ArrayList<>();
    
    /**
     * Row structure : list of column names
     */
    private Set<String> schema = new HashSet<>();
    
    /**
     * Insert a new row into the Datasource
     * The row is expected to be a set of couples (key, value)
     * If the Datasource is empty AND the schema is not set yet,
     * then the first row sets the schema as the unique one to be used
     * 
     * @param row Set of couples (Key, Value)
     *
     * @throws Exception 
     */
    public void insertRow(Map<String, Object> row) throws Exception
    {
        Map<String, Object> newRow = new HashMap<>();

        if (this.schema.isEmpty()) {
            /*
             * Because schema is not initialized
             * Takes the first row structure as schema
             */
            this.schema = row.keySet();
        }
        
        /*
         * For each key in the schema
         * get the value pointed by this key if any
         * and put it into the new row
         * If key is not present in the row, then a null value is added
         */
        Iterator<String> it = this.schema.iterator();
        
        while (it.hasNext()) {
            String currentKey;
            
            currentKey = (String) it.next();
            newRow.put(currentKey, row.get(currentKey));
        }
        
        this.data.add(newRow);
//        throw new Exception("insertRow Not Implemented");
    }
    
    /**
     * Accessor to the internal schema
     * @return
     * @throws Exception 
     */
    public String[] getSchema() throws Exception
    {
        Iterator<String> it = this.schema.iterator();
        int numKey = 0;
        String[] returnedValue = new String[this.schema.size()];
        
        while (it.hasNext()) {
            String currentKey;
            
            currentKey = (String) it.next();
            returnedValue[numKey] = currentKey;
            numKey++;
        }
        return returnedValue;
//        throw new Exception("getSchema Not Implemented");
    }
    /**
     * Set a new schema for the Datasource
     * If schema is already set, then an exception is raised
     * @param schema Schema to be used
     * @throws Exception 
     */
    public void setSchema(String[] schema) throws Exception
    {
        if (!this.schema.isEmpty()) {
            throw new Exception("ERROR : Schema has already been set for this Datasource");
        }
        boolean ok = this.schema.addAll(Arrays.asList(schema));
        
//        throw new Exception("setSchema not implemented");
    }
    
    @Override
    public Iterator<Map<String,Object>> iterator() {
        Iterator<Map<String,Object>> it;
        it = new Iterator<Map<String,Object>>() {
            
            private int currentIndex = 0;

            @Override
            public boolean hasNext()
            {
                boolean valid = (this.currentIndex >= 0);
                valid &= (this.currentIndex < Datasource.this.data.size());
                return valid;
            }

            @Override
            public Map<String, Object> next() {
                return Datasource.this.data.get(this.currentIndex);
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
        return it;
    }
    /**
     * Return the number of items contained in the Datasource
     * 
     * @return int
     */
    public int getCount()
    {
        return this.data.size();
    }
    /**
     * Add a DataCollector
     * When added, the DataCollector.dataCollect() is invoked immediately
     * and the Datasource is expanded with new items
     *
     * @param dataCollector Source of data
     *
     * @throws Exception 
     */
    public void addDataCollector(DataCollectorInterface dataCollector) throws Exception
    {
        this.dataCollectors.add(dataCollector);
    }
    
    /**
     * Process all data from included DataCollectors
     *
     * @return void
     *
     * @throws Exception 
     */
    public void processData() throws Exception
    {
        for (DataCollectorInterface dc : this.dataCollectors) {
            ArrayList<Map<String, Object>> rows = dc.collectData();
            for (Map<String, Object> row : rows) {
                this.insertRow(row);
            }
        }
        
    }
}

