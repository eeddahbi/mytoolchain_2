Feature: Import data in Report Datasource
    In order to have the container Datasource ready
    As an external actor
    I want to store a set of formatted rows

Background:
    Given the datasource is empty

Scenario: Store formatted data with same schema
    When  I add these rows
        |   col1    |   col2    |   col3    |
        | value0101 | value0102 | value0103 |
        | value0201 | value0202 | value0203 |
        | value0301 | value0302 | value0303 |
     Then the data source has exactly 3 rows
     And the schema is "col1,col2,col3" 

# Scenario: Store formatted data with different schema
#     Given the datasource is empty
#     When  I add these rows
#         |   col1    |   col2    |   col3    |   col4    |
#         | value0101 | value0102 | value0103 |           |
#         | value0201 | value0202 | value0203 | value0204 |
#         | value0301 | value0302 | value0303 |           |
#      Then the data source has exactly 3 rows
#      And the schema is "col2,col3,col1" 
# 
# Scenario: Set schema before store data
#     Given I set the schema to "col1" "col2" "col4"
#     When  I add these rows
#         |   col1    |   col2    |   col3    |   col4    |
#         | value0101 | value0102 | value0103 |           |
#         | value0201 | value0202 | value0203 | value0204 |
#         | value0301 | value0302 | value0303 |           |
#     When  I iterate throw the rows from the data source
#     Then all the rows have the schema as "col1" "col2" "col4"

