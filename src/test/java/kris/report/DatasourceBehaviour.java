package kris.report;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.*;

public class DatasourceBehaviour {
    private Datasource datasource;
    
    @Given("^the datasource is empty$")
    public void the_datasource_is_empty() throws Exception {
        // Write code here that turns the phrase above into concrete actions
        this.datasource = new Datasource();
//        throw new PendingException();
    }
    
    @When("^I add these rows$")
    public void i_add_these_rows(DataTable arg1) throws Exception {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
        // E,K,V must be a scalar (String, Integer, Date, enum etc)

        List<Map<String, String>> containor = arg1.asMaps(String.class, String.class);
        containor.forEach((Map<String, String> row) -> {
            Map<String, Object> item = new HashMap<>();
            for (Map.Entry<String, String> entry : row.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();
                // ...
                item.put(key, value);
            }
            try {
                this.datasource.insertRow(item);
            } catch (Exception ex) {
                Logger.getLogger(DatasourceBehaviour.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        //        throw new PendingException();
    }
    
    @Then("^the data source has exactly (\\d+) rows$")
    public void the_data_source_has_exactly_rows(int arg1) throws Exception {
        // Write code here that turns the phrase above into concrete actions
        Assert.assertEquals(arg1, this.datasource.getCount());
//        throw new PendingException();
    }
    
    @Then("^the schema is \"([^\"]*)\"$")
    public void the_schema_is(String arg1) throws Exception {
        // Write code here that turns the phrase above into concrete actions
        String actualSchema = String.join(",", this.datasource.getSchema());
        Assert.assertEquals(arg1, actualSchema);
//        throw new PendingException();
    }
    
    @Then("^I can iterate throw the stored data in the data source$")
    public void i_can_iterate_throw_the_stored_data_in_the_data_source() throws Exception {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
}
