package kris.report;

import io.cucumber.junit.Cucumber;
import org.junit.runner.RunWith;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources/kris/report", plugin = {"pretty"})
public class RunCucumberTest {
	
}